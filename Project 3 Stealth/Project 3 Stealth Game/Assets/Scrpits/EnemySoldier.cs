﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySoldier : MonoBehaviour
{
    public Transform myTarget;
    public float rotationSpeed;
    public float moveSpeed;
    EnemyHearing hearing;
    EnemeyVision sight;
    public GameObject target;
    public GameObject Bullet;
    public Transform firePoint;
    public float currentFireTime;
    public float maxFireTime;
    public float shootRange;

    public enum IAIState
    {
        idle,
        chase,
        patrol,
        reset,
        shooting,
    }
    public IAIState myState;

    // Start is called before the first frame update
    void Start()
    {
        hearing = GetComponent<EnemyHearing>();
        sight = GetComponent<EnemeyVision>();

        myState = IAIState.idle;
    }

    // Update is called once per frame
    void Update()
    {
        switch (myState)
        {
            case IAIState.idle:
                idleState();
                break;
            case IAIState.chase:
                chaseState();
                break;
            case IAIState.patrol:
                patrol();
                break;
            case IAIState.reset:
                reset();
                break;
            case IAIState.shooting:
                shooting();
                break;
            default:
                break;
        }
    }

    public void idleState()
    {
        //do nothing
        myState = IAIState.patrol;
    }

    public void chaseState()
    {
        //chases the player when seen or heard
        if (target != null && (sight.CanSee(target) || hearing.CanHear(target)))
        {
            move();
            rotate();
            if (CloseEnough(target))
            {
                myState = IAIState.shooting;
            }
        }
        else
        {
            myState = IAIState.reset;
        }
    }

    private void rotate()
    {
        Vector3 vectorToTarget = myTarget.position - transform.position;
        float zangle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg - 90;
        Quaternion targetRotation = Quaternion.Euler(0, 0, zangle);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }

    private void move()
    {
        transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);
    }

    public void patrol()
    {
        // rotates in place
        if (sight.CanSee(target) || hearing.CanHear(target))
        {
            myState = IAIState.chase;
        }
    }

    public void reset()
    {
        //resets the ai to idle when the ai cannot see or hear player
        if (target == null || (!sight.CanSee(target) || !hearing.CanHear(target)))
        {
            myState = IAIState.idle;
        }
    }

    public void shooting()
    {
        // when close enough shoot the player\
        checkFireTime();
        if (CloseEnough(target) && target != null)
        {
            if (checkFireTime() == true)
            {
                ShootBullet();
            }
        }
        else
        {
            myState = IAIState.chase;
        }
    }

    private bool CloseEnough(GameObject target)
    {
        if (Vector3.Distance(target.transform.position, transform.position) > shootRange)
        {
            return false;
        }
        return true;
    }

    private bool checkFireTime()
        {
            if (currentFireTime > 0)
            {
                currentFireTime -= Time.deltaTime;
                return false;
            }
            else
            {
                return true;
            }
        }

        private void ShootBullet()
        {
            currentFireTime = maxFireTime;
            Instantiate(Bullet, firePoint.position, firePoint.rotation);
        }
}
