﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float rotateSpeed;
    public float moveSpeed;
    private Transform Transform;
    public Transform firePoint;
    public GameObject Bullet;

    public float currentFireTime;
    public float maxFireTime;

    // Start is called before the first frame update
    void Start()
    {
        Transform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        Transform.Translate(0, (Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime), 0);
        Transform.Rotate(0, 0, (Input.GetAxis("Horizontal") * rotateSpeed * Time.deltaTime));

        checkFireTime();
        if (Input.GetButton("Fire1"))
        {
            if (checkFireTime() == true)
            {
                ShootBullet();
            }
        }
    }

    private bool checkFireTime()
    {
        if (currentFireTime > 0)
        {
            currentFireTime -= Time.deltaTime;
            return false;
        }
        else
        {
            return true;
        }
    }

    private void ShootBullet()
    {
        currentFireTime = maxFireTime;
        Instantiate(Bullet, firePoint.position, firePoint.rotation);
    }

}
