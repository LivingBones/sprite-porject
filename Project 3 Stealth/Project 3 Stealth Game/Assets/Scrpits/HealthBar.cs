﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public float max;
    public float current;
    public Image imageComponent;
    public float damageToTake;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (imageComponent != null)
        {
            imageComponent.fillAmount = current / max; 
        }
    }

    public void takeDamage()
    {
        current -= damageToTake;
        if (current <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
