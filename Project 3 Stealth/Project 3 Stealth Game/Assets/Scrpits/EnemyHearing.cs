﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHearing : MonoBehaviour
{
    public float hearingRange;

    public bool CanHear(GameObject target)
    {
        
         GetComponent<NoiseMaker>();

        float distance = Vector3.Distance(target.transform.position, transform.position);

        if (target.gameObject.GetComponent<NoiseMaker>().volume + hearingRange > distance)
        {
            return true;
        }

        else
        {
            return false;
        }
     }
}
