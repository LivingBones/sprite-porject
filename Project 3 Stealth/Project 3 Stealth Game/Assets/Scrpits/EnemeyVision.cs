﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemeyVision : MonoBehaviour
{
    public float fieldOfView;
    public float sightRange;

    public bool CanSee (GameObject target)
    {
        Vector3 vectorTotarget = target.transform.position - transform.position;
        float angelToTarget = Vector3.Angle(transform.up, vectorTotarget);

        RaycastHit2D hit = Physics2D.Raycast(transform.position, vectorTotarget);

        if (angelToTarget >= fieldOfView)
        {
            return false;
        }
        if (Vector3.Distance(target.transform.position, transform.position) >= sightRange)
        {
            return false;
        }
        if (hit.collider == null)
        {
            return false;
        }
        if (hit.collider.gameObject == target)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
