﻿using UnityEngine;

public class BulletScript : MonoBehaviour
{
    private Transform Transform;
    public float delayBeforeDestroy;

    public float moveSpeed;

    // Start is called before the first frame update
    void Start()
    {
        Transform = GetComponent<Transform>();
        Destroy(this.gameObject, delayBeforeDestroy);
    }

    // Update is called once per frame
    void Update()
    {
        Transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
 
    {
        if (collision.gameObject.GetComponent<EnemySoldier>() != null || collision.gameObject.GetComponent<PlayerControl>() != null) 
        {
            collision.gameObject.GetComponent<HealthBar>().takeDamage();
            Destroy(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
