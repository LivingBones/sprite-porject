﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource source;
    public AudioClip pickUp;
    private BoxCollider2D bc2d;
    private SpriteRenderer sr;
    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
        bc2d = GetComponent<BoxCollider2D>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            source.clip = pickUp;
            source.Play();
            sr.enabled = false;
            bc2d.enabled = false;
        }
    }
}
