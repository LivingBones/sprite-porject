﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float speed;
    SpriteRenderer sr;
    Rigidbody2D rb2d;
    public int jumpMax;
    private int jumpsCurrent;
    public float jumpForce;
    private Transform tf;
    public float groundCheckDistance;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        rb2d = GetComponent<Rigidbody2D>();
        jumpsCurrent = jumpMax;
        tf = GetComponent<Transform>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //sr.flipX = rb2d.velocity.x < 0;

        Movement();
        if (Input.GetButtonDown("Jump") && jumpsCurrent > 0)
        {
            Jump();
        }
        else
        {
            checkResetJump();
        }
    }

    private void Movement()
    {
        float xMove = Input.GetAxis("Horizontal") * speed;
        rb2d.velocity = new Vector2(xMove, rb2d.velocity.y);
        if (xMove > 0 || xMove < 0)
        {
            anim.SetBool("isRunning", true); 
        }
        else
        {
            anim.SetBool("isRunning", false);
        }
    }

    private void Jump()
    {
        jumpsCurrent -= 1;
        rb2d.velocity = new Vector2(rb2d.velocity.x, Vector2.up.y * jumpForce);

    }

    private void checkResetJump()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(tf.position, Vector2.down, groundCheckDistance);
        // Debug To Draw distance
        //Debug.DrawRay(tf.position, Vector2.down * groundCheckDistance, Color.red);
        if (hitInfo.collider != null)
        {
            resetJump();
        }
    }

    private void resetJump()
    {
        jumpsCurrent = jumpMax;
    }
}
