﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Transform theTransform;
    private float moveHorizontal;
    private float moveVertical;

    public float speed;


    // Start is called before the first frame update
    void Start()
    {
        theTransform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.RightArrow)) {
               Vector3 movement = new Vector3(1f, 0f, 0f);
                theTransform.position = theTransform.position + movement;
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                Vector3 movement = new Vector3(-1f, 0f, 0f);
                theTransform.position = theTransform.position + movement;
            }

            if(Input.GetKeyDown(KeyCode.UpArrow))
            {
                Vector3 movement = new Vector3(0f, 1f, 0f);
                theTransform.position = theTransform.position + movement;
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                Vector3 movement = new Vector3(0f, -1f, 0f);
                theTransform.position = theTransform.position + movement;
            }
        }

        else {
            moveHorizontal = Input.GetAxis("Horizontal");
            moveVertical = Input.GetAxis("Vertical");
            Vector3 movement = new Vector3(moveHorizontal, moveVertical, 0);
            theTransform.position = theTransform.position + (movement * speed);
        }
    }
}
