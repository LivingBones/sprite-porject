﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShips : MonoBehaviour
{
    public Transform myTarget;
    public Vector3 directionToPlayer;
    public float moveSpeed;
    // Start is called before the first frame update
    void Start()
    {
        myTarget = FindObjectOfType<PlayerShipControl>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (myTarget != null)
        {
            directionToPlayer = myTarget.position - transform.position;
            float angleToTarget = Mathf.Atan2(directionToPlayer.y, directionToPlayer.x) * Mathf.Rad2Deg - 90;
            transform.rotation = Quaternion.Euler(0, 0, angleToTarget);
            transform.Translate(Vector3.up * Time.deltaTime * moveSpeed); 
        }
        else
        {
            myTarget = FindObjectOfType<PlayerShipControl>().transform;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<PlayerShipControl>() != null)
        {
            GameManager.instance.clearEnemies();
            GameManager.instance.spawnPlayer();
            GameManager.instance.spawnNewWave();
            Destroy(collision.gameObject);
        }
    }
}
