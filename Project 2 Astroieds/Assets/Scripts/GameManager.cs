﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public int score = 0;
    public Vector3 playerStart;
    public Spawner spawner;
    public List<GameObject> spawnedEnemies;

    public int playerLives;
    public GameObject playerObj;

    private void Awake()
    {
        #region Singleton
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        } 
        #endregion
    }

    public void spawnPlayer()
    {
        playerLives--;
        if (playerLives > 0)
        {
            Instantiate(playerObj, playerStart, playerObj.transform.rotation);
        }
        else
        {
            Application.Quit();
        }
    }

    public void clearEnemies()
    {
        int myNum = spawnedEnemies.Count;
        for (int i = 0; i < myNum; i++)
        {
            Debug.Log(i);
            Destroy(spawnedEnemies[i]);
        }
        spawnedEnemies.Clear();
    }

    public void spawnNewWave()
    {
        for (int i = 0; i < 3; i++)
        {
            spawner.spawnObj();
        }
    }
}
