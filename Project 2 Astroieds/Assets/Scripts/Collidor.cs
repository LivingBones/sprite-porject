﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collidor : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<PlayerShipControl>() != null)
        {
            GameManager.instance.clearEnemies();
            GameManager.instance.spawnPlayer();
            GameManager.instance.spawnNewWave();
            Destroy(collision.gameObject); 
        }
        else if (collision.gameObject.GetComponent<EnemyShips>() != null || collision.gameObject.GetComponent<Astroids>() != null)
        {
            GameManager.instance.spawnedEnemies.Remove(collision.gameObject);
            GameManager.instance.spawner.spawnObj();
            Destroy(collision.gameObject);
        }
    }
}
