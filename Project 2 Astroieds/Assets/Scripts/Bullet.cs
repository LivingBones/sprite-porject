﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Transform m_Transfrom;
    public float moveSpeed;
    public float delayBeforeDestroy;

    // Start is called before the first frame update
    void Start()
    {
        //destroys bullet after a set time
        m_Transfrom = GetComponent<Transform>();
        Destroy(this.gameObject, delayBeforeDestroy);
    }

    // Update is called once per frame
    void Update()
    {
        // makes that bullet sprite move forward
        m_Transfrom.Translate(Vector3.right * moveSpeed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //destroys objects once collided
        if (collision.gameObject.GetComponent<EnemyShips>() != null || collision.gameObject.GetComponent<Astroids>() != null)
        {
            GameManager.instance.spawnedEnemies.Remove(collision.gameObject);
            GameManager.instance.spawner.spawnObj();
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
        else
	    {
	        Destroy(this.gameObject); 
	    }
    }
}
