﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipControl : MonoBehaviour
{
    public float rotateSpeed;
    public float moveSpeed;
    private Transform m_Transform;
    private Rigidbody2D m_Rigidbody2D;
    public Transform firepoint;
    public GameObject bullet;

    public float currentFireTime;
    public float maxFireTime;


    // Start is called before the first frame update
    void Start()
    {
        m_Transform = GetComponent<Transform>();
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //movemnt of the ship
        m_Transform.Translate(0, (Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime), 0);
        m_Transform.Rotate(0, 0, Input.GetAxis("Horizontal") * rotateSpeed * Time.deltaTime);

        checkFireTime();
        if (Input.GetButton("Fire1"))
        {
            if (checkFireTime() == true)
            {
                Shootbullet(); 
            }
        }
    }

    private bool checkFireTime()
    {
        if (currentFireTime > 0)
        {
            currentFireTime -= Time.deltaTime;
            return false;
        }
        else
        {
            return true;
        }
    }

    private void Shootbullet()
    {
        currentFireTime = maxFireTime;
        Quaternion offsetAngle = Quaternion.Euler(0,0,90);
        Instantiate(bullet, firepoint.position, m_Transform.rotation * offsetAngle);
    }
}
