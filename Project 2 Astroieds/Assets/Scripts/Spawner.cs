﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public List<GameObject> enemiesList;
    public List<Transform> spawnPoints;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.spawner = this;

        for (int i = 0; i < 3; i++)
        {
            spawnObj();
        }

    }

    public void spawnObj()
    {
        int randObj = Random.Range(0, enemiesList.Count);
        int randPoint = Random.Range(0, spawnPoints.Count);
        GameManager.instance.spawnedEnemies.Add(Instantiate(enemiesList[randObj], spawnPoints[randPoint]));
    }
}
