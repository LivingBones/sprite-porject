﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astroids : MonoBehaviour
{
    public Transform myTarget;
    public Vector3 directionToPlayer;
    public float moveSpeed;
    // Start is called before the first frame update
    void Start()
    {
        //on start moves towards player position that it spawned on
        myTarget = FindObjectOfType<PlayerShipControl>().transform;
        directionToPlayer = myTarget.position - transform.position;
        float angleToTarget = Mathf.Atan2(directionToPlayer.y,directionToPlayer.x) * Mathf.Rad2Deg - 90;
        transform.rotation = Quaternion.Euler(0, 0, angleToTarget);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * Time.deltaTime * moveSpeed);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // once collided with player ship destroys it
        if (collision.gameObject.GetComponent<PlayerShipControl>() != null)
        {
            GameManager.instance.clearEnemies();
            GameManager.instance.spawnPlayer();
            GameManager.instance.spawnNewWave();
            Destroy(collision.gameObject);
        }
    }
}
